=================================
Migrating Cloud Modules From Salt
=================================

Idem can use cloud execution and state modules from Salt with minimal effort.
This guide will go over the main items required to make the transition, but
definitely check out the `Migrating Support From Salt <migrate_salt.html>`_ docs
as well.


Idem Providers Are Plugin Subsystems via POP
============================================

Unlike Salt's loader, `POP <https://pop.readthedocs.io/en/latest/>`_ allows for
nested plugin subsystems in Idem. This gives us two great benefits:

1. Each cloud provider can be housed in its own git repository, allowing it to
   be tested separately and released on its own cadence. POP will merge the
   provider into Idem when you :code:`pip install` the provider.
2. Idem recursively loads modules in the :code:`exec` and :code:`states`
   directories. This means that you can move Salt's :code:`modules` and
   :code:`states` plugins into sub-directories when they transition to Idem!

When porting a module called :code:`salt/modules/boto_s3.py`, it could be moved
to :code:`exec/aws/s3.py` or :code:`exec/aws/storage/s3.py`. The location of the
file is reflected on the hub as :code:`hub.exec.aws.s3` or
:code:`hub.exec.aws.storage.s3` respectively.

In the context of the Microsoft Azure provider, the large modules in this tree:

.. code-block::

    modules/azurearm_compute.py
    modules/azurearm_dns.py
    modules/azurearm_network.py
    modules/azurearm_resource.py

    states/azurearm_compute.py
    states/azurearm_dns.py
    states/azurearm_network.py
    states/azurearm_resource.py

...got broken out into smaller, more manageable chunks of code when they moved
to Idem:

.. code-block::

    exec/azurerm/compute/availability_set.py
    exec/azurerm/compute/disk.py
    exec/azurerm/compute/image.py
    exec/azurerm/compute/virtual_machine.py
    exec/azurerm/dns/record_set.py
    exec/azurerm/dns/zone.py
    exec/azurerm/network/__init__.py
    exec/azurerm/network/load_balancer.py
    exec/azurerm/network/local_network_gateway.py
    exec/azurerm/network/network_interface.py
    exec/azurerm/network/network_security_group.py
    exec/azurerm/network/public_ip_address.py
    exec/azurerm/network/route.py
    exec/azurerm/network/virtual_network_gateway.py
    exec/azurerm/network/virtual_network_peerings.py
    exec/azurerm/network/virtual_network.py
    exec/azurerm/resource/deployment.py
    exec/azurerm/resource/group.py
    exec/azurerm/resource/management_lock.py
    exec/azurerm/resource/policy.py
    exec/azurerm/resource/provider.py
    exec/azurerm/resource/subscription.py
    exec/azurerm/resource/tenant.py

    states/azurerm/compute/availability_set.py
    states/azurerm/compute/virtual_machine.py
    states/azurerm/dns/record_set.py
    states/azurerm/dns/zone.py
    states/azurerm/network/load_balancer.py
    states/azurerm/network/local_network_gateway.py
    states/azurerm/network/network_interface.py
    states/azurerm/network/network_security_group.py
    states/azurerm/network/public_ip_address.py
    states/azurerm/network/route.py
    states/azurerm/network/virtual_network_gateway.py
    states/azurerm/network/virtual_network_peering.py
    states/azurerm/network/virtual_network.py
    states/azurerm/resource/group.py
    states/azurerm/resource/policy.py

Idem still follows the same constructs as Salt in separating ad-hoc execution
functionality from idempotent enforcement in two separate subsystems. The idea
is that these are separate concerns and that raw execution presents value in
itself making the code more reusable.


No More Loader
==============

Since the traditional Salt loader is gone, we don't have any use for
:code:`__virtualname__` or the :code:`def __virtual__()` function. Blocks like
this can be removed:

.. code:: python

    __virtualname__ = "azurearm_resource"

.. code:: python

    def __virtual__():
        if not HAS_LIBS:
            return (
                False,
                "The following dependencies are required to use the AzureARM "
                "modules: Microsoft Azure SDK for Python >= 2.0rc6, "
                "MS REST Azure (msrestazure) >= 0.4",
            )

        return __virtualname__


Function Definition Differences
===============================

All function calls now need to accept the hub as the first argument. Functions
should also be changed to be :code:`async` functions where appropriate.


Exec Function Calls
-------------------

 So, this `exec` function signature:

.. code-block:: python

    def resource_group_check_existence(name, **kwargs):
        ...

...gets changed to look like this:

.. code-block:: python

    async def check_existence(hub, name, **kwargs):
        ...


States Function Calls
---------------------

States function calls now accept a :code:`ctx` argument. This allows us to send
an execution context into the function. The :code:`ctx` is a dict with the keys
:code:`test` and :code:`run_name`. The :code:`test` value is a Boolean telling
the state if it is running is test mode. The :code:`run_name` is the name of the
run as it is stored on the hub, using the :code:`run_name` you can gain access
to the internal tracking data for the execution of the Idem run located in
:code:`hub.idem.RUNS[ctx['run_name']]`.

So, a state function signature that looks like this in Salt:

.. code-block:: python

    def resource_group_present(
        name, location, managed_by=None, tags=None, connection_auth=None, **kwargs
    ):
        ...


...will look like this in Idem:

.. code-block:: python

    async def present(
        hub, ctx, name, location, managed_by=None, tags=None, connection_auth=None, **kwargs
    ):
        ...


Where Are My Dunders?
=====================

Salt modules often have references to :code:`__utils__` and :code:`__salt__` in
order to reference general utility or execution modules respectively. Instead,
Idem plugins will reference those modules on the hub. Additionally, you'll need
to :code:`await` the calls if they are :code:`async` functions.

Execution module referencing :code:`__utils__`:

.. code:: python

    resconn = __utils__["azurearm.get_client"]("resource", **kwargs)

.. code:: python

    resconn = await hub.exec.utils.azurerm.get_client("resource", **kwargs)

State module referencing :code:`__salt__`:

.. code:: python

    present = __salt__["azurearm_resource.resource_group_check_existence"](
        name, **connection_auth
    )

.. code:: python

    present = await hub.exec.azurerm.resource.group.check_existence(name, **connection_auth)


Other Salty Things
==================

In moving functionality over to Idem from Salt, there are a few other things to
watch out for.

- Check your imports. You probably have some references to Salt libraries which
will need to be removed or replaced.

- Look for custom exceptions. You might have some handlers for
:code:`SaltInvocationError`, :code:`SaltSystemExit`, or other Salt exceptions
which will need to be handled differently.


The Hard Part: Virtual Machine Functions
========================================

If you've completed everything above, you should be pretty close to having some
working cloud execution and state modules in Idem! Unfortunately, you may have
noticed that those modules were for cloud infrastructure or services but didn't
include virtual machine instance creation, modification, or deletion. These
functions were traditionally handled by Salt Cloud.

Cloud instance creation in Salt Cloud is very much a different animal. It
doesn't handle idempotent modification of instances. If an instance name is
found in the cloud provider, Salt Cloud doesn't check it to see if it's drifted
from the definition in the instance profile.

Due to this key difference, the Azure instance functions in Idem were basically
written from scratch using the Salt Cloud functions as a reference. Your mileage
may vary depending on the specific cloud provider you're looking at porting, but
take care to model the functions more after the Idem state functions you've
ported more than the Salt Cloud functions that you're referencing. Check out the
`Microsoft Azure virtual machine state functions
<https://github.com/eitrtechnologies/idem_provider_azurerm/blob/master/idem_provider_azurerm/states/azurerm/compute/virtual_machine.py>`_
as a reference.
