Notes
=====

Please, please, please use async libraries as much possible when implementing clouds in idem.
Making HTTP requests is the throttle point of this application.
Using something like aiohttp_ under the hood for your api requests means other idem clouds won't be set back waiting
for your cloud's operations to complete when running multi-cloud sls files.

.. _aiohttp: https://docs.aiohttp.org/en/stable/

Common Pitfalls

1. It's still relatively new in Python and a lot of people aren't including it in their projects.

2. Putting all code into one monolithic file.
You can put everything in one file, but please don't. It will compile; it will work,
but it's not going to be intuitive to users; it's not going to be easy to maintain
or contribute to.
So, compartmentalize in a way that makes sense for your cloud.

3. Doing what works now and saving the right way for later.
