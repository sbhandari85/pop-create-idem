==========
Idem Cloud
==========

The advantage to idem-cloud is that you can have a standardized CLI interface for every cloud.
With `acct` profiles, credentials for accessing your cloud are encrypted --
no more plaintext credentials on your file system or in your shell history!
`idem-cloud` states work together seamlessly between cloud providers and profiles;
Your instance from one cloud provider can depend on the existence of a vpc from a completely different cloud provider.
In this document we will explore the structure of an idem-cloud project so that you can easily create your own!
