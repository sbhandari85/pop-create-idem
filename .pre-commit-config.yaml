---
minimum_pre_commit_version: 2.10.1
repos:
  - repo: https://github.com/pre-commit/pre-commit-hooks
    rev: v4.0.1
    hooks:
      - id: check-merge-conflict
        description: Check for files that contain merge conflict strings.
        language_version: python3
      - id: trailing-whitespace
        description: Trims trailing whitespace.
        args: [--markdown-linebreak-ext=md]
        language_version: python3
      - id: mixed-line-ending
        description: Replaces or checks mixed line ending.
        args: [--fix=lf]
        exclude: make.bat
        language_version: python3
      - id: fix-byte-order-marker
        description: Removes UTF-8 BOM if present, generally a Windows problem.
        language_version: python3
      - id: end-of-file-fixer
        description: Makes sure files end in a newline and only a newline.
        exclude: tests/fake_.*\.key
        language_version: python3
      - id: check-ast
        exclude: ({{cookiecutter.root_dir}}/.*)
        description: Simply check whether files parse as valid python.
        language_version: python3
      - id: check-yaml
      - id: check-json

  # ----- Formatting ---------------------------------------------------------------------------->
  - repo: https://github.com/myint/autoflake
    rev: v1.4
    hooks:
      - id: autoflake
        exclude: ({{cookiecutter.root_dir}}/.*)
        name: Remove unused variables and imports
        language: python
        args: ["--in-place", "--remove-all-unused-imports", "--remove-unused-variables", "--expand-star-imports"]
        files: \.py$

  - repo: https://github.com/saltstack/pre-commit-remove-import-headers
    rev: 1.1.0
    hooks:
      - id: remove-import-headers

  - repo: https://github.com/asottile/pyupgrade
    rev: v3.1.0
    hooks:
      - id: pyupgrade
        name: Rewrite Code to be Py3.7+
        args: [--py37-plus]

  - repo: https://github.com/asottile/reorder_python_imports
    rev: v2.6.0
    hooks:
      - id: reorder-python-imports
        exclude: ({{cookiecutter.root_dir}}/.*)
        args: [--py37-plus]

  - repo: https://github.com/psf/black
    rev: 22.3.0
    hooks:
      - id: black
        args: []
        exclude: ({{cookiecutter.root_dir}}/.*)

  - repo: https://github.com/asottile/blacken-docs
    rev: v1.12.1
    hooks:
      - id: blacken-docs
        exclude: >
            (?x)^(
                pop_create_idem/cloudspec/template/.*
            )$
        additional_dependencies: [black==22.3.0]

  - repo: https://github.com/myint/rstcheck
    rev: 3f92957
    hooks:
      - id: rstcheck
        name: Check reST files using rstcheck
        args: [--report=warning]
        additional_dependencies: [sphinx]
  # <---- Formatting -----------------------------------------------------------------------------

  # ----- Testing Static Requirements ------------------------------------------------------------------------------------>
  - repo: https://github.com/saltstack/pip-tools-compile-impersonate
    rev: '4.1'
    hooks:
      - id: pip-tools-compile
        alias: compile-3.7-test-requirements
        name: Py3.7 Test Requirements
        files: ^requirements/tests.in$
        pass_filenames: false
        args:
          - -v
          - --py-version=3.7
          - --platform=linux
          - requirements/tests.in

      - id: pip-tools-compile
        alias: compile-3.8-test-requirements
        name: Py3.8 Test Requirements
        files: ^requirements/tests.in$
        pass_filenames: false
        args:
          - -v
          - --py-version=3.8
          - --platform=linux
          - requirements/tests.in

      - id: pip-tools-compile
        alias: compile-3.9-test-requirements
        name: Py3.9 Test Requirements
        files: ^requirements/tests.in$
        pass_filenames: false
        args:
          - -v
          - --py-version=3.9
          - --platform=linux
          - requirements/tests.in

      - id: pip-tools-compile
        alias: compile-3.10-test-requirements
        name: Py3.10 Test Requirements
        files: ^requirements/tests.in$
        pass_filenames: false
        args:
          - -v
          - --py-version=3.10
          - --platform=linux
          - requirements/tests.in

  # <---- Testing Static Requirements -------------------------------------------------------------------------------------

  # ----- Docs -------------------------------------------------------------------------------------------------->

  - repo: https://github.com/saltstack/mirrors-nox
    rev: v2020.8.22
    hooks:
      - id: nox
        alias: Generate Sphinx docs
        description: Generate Sphinx docs, ensure they build
        files: ^((CONTRIBUTING|README)|docs/.*)\.rst$
        args:
          - -e
          - 'docs-html(clean=True)'
          - --

  # <---- Docs ---------------------------------------------------------------------------------------------------
