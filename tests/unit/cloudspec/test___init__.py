from cloudspec import CloudSpecParam


def test_cloudspecparam_with_nested_cloudspecparam():
    nested_param = CloudSpecParam(
        name="nestedParam",
        required=True,
        target="kwargs",
        target_type="mapping",
        member=None,
        param_type="str",
        doc="Some doc",
        default="default-value",
    )
    wrapping_param = CloudSpecParam(
        name="wrappingParam",
        required=True,
        target="kwargs",
        target_type="mapping",
        member={"name": "WrappingParamType", "params": {"nestedParam": nested_param}},
        param_type="{}",
        doc="Some doc for the wrapper",
        default=None,
    )
    assert wrapping_param == CloudSpecParam(
        name="wrappingParam",
        required=True,
        target="kwargs",
        target_type="mapping",
        member={
            "name": "WrappingParamType",
            "params": {
                "nestedParam": {
                    "required": True,
                    "target": "kwargs",
                    "target_type": "mapping",
                    "member": None,
                    "param_type": "str",
                    "doc": "Some doc",
                    "default": "default-value",
                }
            },
        },
        param_type="{}",
        doc="Some doc for the wrapper",
        default=None,
    )


def test_cloudspecparam_snaked_overrides_default():
    nested_param = CloudSpecParam(
        name="param-name",
        required=True,
        target="kwargs",
        target_type="mapping",
        member=None,
        param_type="str",
        doc="Some doc",
        default="default-value",
        snaked="overridden-snaked",
    )
    assert nested_param.snaked == "overridden-snaked"


def test_cloudspecparam_no_snaked():
    nested_param = CloudSpecParam(
        name="nestedParam",
        required=True,
        target="kwargs",
        target_type="mapping",
        member=None,
        param_type="str",
        doc="Some doc",
        default="default-value",
    )
    assert nested_param == {
        "required": True,
        "target": "kwargs",
        "target_type": "mapping",
        "member": None,
        "param_type": "str",
        "doc": "Some doc",
        "default": "default-value",
        "snaked": "nested_param",
    }
